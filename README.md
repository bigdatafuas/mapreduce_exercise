# Init the project (already prepared)
The maven project was initialized with
```
mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=com.bigdata.sum -DartifactId=sum
```

# Requirements (already installed in the provided template) 
- maven
- hadoop installed in `~/hadoop-3.3.1`
- python3-pip
- faker
The python package faker is only required to generate the input data.
If not already done, on Ubuntu 20.04 can be installed with
```
sudo apt install -y openjdk-11-jdk maven python3-pip
pip3 install faker
wget https://downloads.apache.org/hadoop/common/hadoop-3.3.1/hadoop-3.3.1.tar.gz
tar xvzf hadoop-3.3.1.tar.gz
```

# Generate the data
Generate some input data at the base directory of the project
```
python3 generate_data.py > input.txt
```

# Build and run the project
```
# export JAVA_HOME
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
# change the folder sum
cd sum
# build the project
mvn clean install
# remove output directory if exists
rm -rf wcout/ 
# run the job
~/hadoop-3.3.1/bin/hadoop jar target/sum-1.0-SNAPSHOT.jar com.bigdata.sum.Sum ../input.txt wcout

```
The result is in the subdir wcout
