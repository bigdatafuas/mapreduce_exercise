from faker import Faker
from collections import OrderedDict
import random
locales = OrderedDict([
    ('de-DE', 1)
])
fake = Faker(locales)
#<date> <time> <city> <salesperson_id> <volume>
Faker.seed(0)
for _ in range(100):
    city = fake.city()
    if ' ' not in city: 
        print(fake.date_time().strftime("%m/%d/%Y,%H:%M:%S"), city, random.randint(0, 10),random.randint(0, 9999), sep=',')
